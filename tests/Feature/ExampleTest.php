<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Redis;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Redis::command('flushall');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    function it_can_use_factory_to_create_user()
    {
        $this->assertCount(0, User::all());

        factory(User::class)->create();

        $this->assertCount(1, User::all());
    }

    /** @test */
    function it_can_use_gitlab_redis_service()
    {
        $this->assertNull(Redis::get('foo'));

        Redis::set('foo', 'bar');

        $this->assertNotNull(Redis::get('foo'));
        $this->assertEquals('bar', Redis::get('foo'));
    }
}
